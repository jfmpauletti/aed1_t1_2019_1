
#include <stdio.h>
#include <stdlib.h>


#include "ls.h"


struct list * createlista(int max){
		if (max <= 0) {
			return NULL;
		}
		struct list *lista;
		
		lista = (struct list*) malloc(sizeof(struct list));
		
		if(lista == NULL){
			printf("NÃO ALOCADO");
			return NULL;
		}
		else{
			
			lista->ultimo = 0;
			lista->capacidade = max;
			lista->arm = malloc(sizeof(elem)*max);
			if(lista->arm == NULL){
				printf("NÃO ALOCADO");
				free(lista);
				return NULL;
			}
		}
		return lista;
}
	
	
	
	
int insert(struct list *desc, int pos, elem item){
	
	if(desc->ultimo >= desc->capacidade) {
		printf("Erro! Lista Cheia");
		return 0;
	}
	if((pos > desc->ultimo + 1)||(pos < 1)){
		printf("ERRO! Posicao inexistente");
		return 0;
	}
	for(int q = desc->ultimo; q >= pos; q--){
		desc->arm[q+1] = desc->arm[q];
	}
	desc->arm[pos] = item;
	desc->ultimo += 1;
	return 1;
}

int removel(struct list *desc, int pos){
	
	if(pos > desc->ultimo){
		printf("Elemento de posição p inexistente");
		return 0;
	}
	desc->ultimo -= 1;
	
	for(int q = pos; q <= desc->ultimo; q++){
		desc->arm[q] = desc->arm[q+1];
	}
	return 1;
}
	
elem get(struct list *desc, int pos){
	
	if((pos > desc->ultimo)||(pos < 1)){
			printf("ERRO! Lista nao contem elemento p");
			return 0;
					}
	else
		return desc->arm[pos];
}

int set(struct list *desc, int pos, elem item){
	
	if((pos > desc->ultimo)||(pos < 1)){
			printf("ERRO! Lista nao contem elemento p");
			return 0;
					}
	else{
			desc->arm[pos] = item;
			return 1;
		
		}
	}

int locate(struct list *desc, int pos, elem item){
	for(int q = pos; q <= desc->ultimo; q++){
		if(desc->arm[q] == item){
				return q;
		}
	}
	return 0;
}

int length(struct list *desc){
	
	return desc->ultimo;
	
	}

int max(struct list *desc){
	
	return desc->capacidade;

}

int full(struct list *desc){
	
	if(max(desc)==length(desc)){
		return 0;
	}
	else
		return 1;
	
	}
	
void destroyLista(struct list *desc){
	
	free(desc);

	}
