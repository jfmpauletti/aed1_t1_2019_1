#include <stdio.h>
#include <stdlib.h>
#include "pilha.h"


/** Cria uma pilha
 * @return  um descritor ou NULL
 */
struct pilha * create(){

	
	struct list *lista = createlista(100);
	struct pilha *p = malloc(sizeof(p));
	
	p->lista = lista;
	
	return p;
}

/** Apaga todos elementos da pilha
 * @param p descritor da pilha
 * @return 1 se OK, 0 se erro
 */
int makenull(struct pilha * p){

	while(!vazia(p)){0

		removel(p->lista, 1);

	}

	if(vazia(p)){
		return 1;

	}return 0;
}

/* Retorna o elemento no topo da pilha, ou zero se não existir
 * @param p descritor da pilha
 * @return o elemento ou 0
 */
int top(struct pilha * p){

	return get(p->lista, 1);
}

/* Descarta o topo da pilha
 * @param p descritor de pilha
 * @return 1 se OK, 0 se erro
 */
int pop(struct pilha * p){

	return removel(p->lista, 1);

}

/* Insere um elemento no topo da pilha
 * @param p descritor de pilha
 * @param val elemento a ser inserido
 * @return 1 se OK, 0 se erro
 */
int push(struct pilha * p, int val){

 	return insert(p->lista, 1 , val);

}
/* Retorna se a pilha está vazia ou não
 * @param p descritor de pilha
 * @return 1 se vazia, 0 se não
 */
int vazia(struct pilha *p){

	if(!get(p->lista, 1)){
		return 1;
	} return 0;

}
/** Desaloca toda a pilha
  * @param p descritor da pilha
  */
void destroy(struct pilha * p){

	destroyLista(p->lista);
}
