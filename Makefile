.PHONY: ls le pilha filas all clean


all: ls le pilha filas

ls:
	$(MAKE) -C 1_lista_sequencial

le:
	$(MAKE) -C 2_lista_encadeada

pilha:	
	$(MAKE) -C 3_pilha

filas:
	$(MAKE) -C 4_filas
clean:
	$(MAKE) clean -C 1_lista_sequencial
	$(MAKE) clean -C 2_lista_encadeada
	$(MAKE) clean -C 3_pilha
	$(MAKE) clean -c 4_filas
