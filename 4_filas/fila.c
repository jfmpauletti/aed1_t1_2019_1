#include "fila.h"
#include <stdio.h>
#include <stdlib.h>

struct fila * create(){
	
	
	struct llist *lista = create_l();
	struct fila *f = malloc(sizeof(f));
	
	f->lista = lista;

	return f;

}

/** Apaga todos elementos da fila
 * @param p descritor da fila
 * @return 1 se OK, 0 se erro
 */

int makenull(struct fila * f){

	while(f->lista->cabeca != NULL ){

		delete_l(f->lista, NULL);

	}
	if(f->lista->cabeca == NULL){

		return 1;

	}else return 0;
}

/* Retorna o elemento mais antigo da fila ou zero se não existir
 * @param p descritor da fila
 * @return o elemento ou 0
 */

int dequeue(struct fila * f){

	elem *aux;
	
	if(get_l(f->lista, 1) == NULL) return 0;
	
	aux = get_l(f->lista, 1);

	f->lista->cabeca = aux->next;

	int i = aux->val;

	free(aux);

	return i;

}

/* Insere um elemento no fim da fila
 * @param p descritor de fila
 * @param val elemento a ser inserido
 * @return 1 se OK, 0 se erro
 */

int enqueue(struct fila * f, int val){
	
	elem *aux;
	aux = create_node(val);
	
	int tam;

	tam = length_l(f->lista);
	
	elem *prevv;
	
	prevv = get_l(f->lista, tam);
	
	return insert_l(f->lista, prevv, aux);

}

/* Retorna se a fila está vazia ou não
 * @param p descritor de fila
 * @return 1 se vazia, 0 se não
 */

int vazia(struct fila * f){

 	if(f->lista->cabeca == NULL){
 		return 1;
 	}return 0;
 }
/** Desaloca toda a fila
  * @param p descritor da fila
  */

void destroy(struct fila * f){

	destroy_l(f->lista);

}
