#include "le.h"

#include <stdlib.h>
#include <stdio.h>


struct llist * create_l() {
	struct llist *ptr = malloc(sizeof(struct llist));
	if (ptr == NULL) return NULL;
	ptr->tam = 0;
	ptr->cabeca = NULL;
	return ptr;
}
	
	
 elem * create_node(int val){
 	elem *nodo = malloc(sizeof(elem));
	if (nodo == NULL) return NULL;
 	nodo->val = val;
	nodo->next = NULL;
	return nodo;
}

int insert_l(struct llist *desc, elem * prev, elem * item){
   if(prev == NULL) {
      item->next = desc->cabeca;
      desc->cabeca = item;
   } else {
	   item->next = prev->next;
	   prev->next = item;
	}
   desc->tam += 1;
	return 1;
	
}

int delete_l(struct llist *desc, elem * prev){

   if(desc == NULL) return 0;
   
   if(prev == NULL){
      elem *ptr = desc->cabeca;
      desc->cabeca = desc->cabeca->next;
      free(ptr);
   } else {
   
      elem *ptr = prev->next;
      prev->next = prev->next->next;
      free(ptr);
      
   }
   desc->tam -= 1;
   return 1;

}



elem * get_l(struct llist *desc, int pos){
   if (desc == NULL) return NULL;
   if (pos > desc->tam) return NULL;
   
   elem *ptr = desc->cabeca;
  
   for(int p = 1; p < pos ; p++ ){
      ptr = ptr->next;
   }
   
   return ptr;

}


int set_l(struct llist *desc, int pos, int val){

   elem *ptr = get_l(desc, pos);
   if (ptr == NULL) return 0;
   ptr->val = val;
   return 1;

}

elem * locate_l(struct llist *desc, elem * prev, int val){
  
   elem *ptr = prev ? prev : desc->cabeca;
   
   while(ptr != NULL && ptr->val != val) {
      ptr = ptr->next;
   }
   

   return ptr;
}

int length_l(struct llist *desc){
   
   return desc ? desc->tam : -1;
   
}

void destroy_l(struct llist *desc){
   
   elem *temp;
   for(elem *ptr = desc->cabeca; ptr != NULL; ptr = temp){
   
      temp = ptr->next;
      free(ptr);
   
   }

}


